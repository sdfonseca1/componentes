import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date-time',
  templateUrl: './date-time.page.html',
  styleUrls: ['./date-time.page.scss'],
})
export class DateTimePage implements OnInit {

  fechaNaci: Date = new Date();
  customPickerOptions;
  customDate;

  constructor() { }

  ngOnInit() {
    this.customPickerOptions = {
      buttons: [
        {
          text: 'Save',
          handler: ( evento ) => {
            console.log('Click Saved');
            console.log(evento);
          }
        },
        {
          text: 'log',
          handler: () => {
            console.log('Clicked log. Do not miss')
            return false;
          }
        }
      ]
    }
  }
  
  cambioFecha( event ){
    console.log('ionChange', event);
    console.log('Date: ', new Date( event.detail.value ));
  }



}
