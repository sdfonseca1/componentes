import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss'],
})
export class LoadingPage implements OnInit {

  loading;

  constructor( private loadingController: LoadingController) { }

  ngOnInit() {

    setTimeout(() => {
      this.loading.dismiss();
    }, 1500);

    this.presentLoading('Cargando, espere por favor...');
  }

  async presentLoading( message: string) {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message
    });

    await this.loading.present();

    const { role, data } = await this.loading.onDidDismiss( () => {
      console.log('On did dismiss close');
    });
    
  }


}
