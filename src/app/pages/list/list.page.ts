import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';
import { IonList, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  usuarios: Observable<any>;
  @ViewChild('lista') lista: IonList;

  constructor( private dataService: DataService,
               private toastController: ToastController ) { }

  ngOnInit() {
    // this.dataService.getUsers().subscribe(console.log);
    this.usuarios = this.dataService.getUsers();
    // console.log(this.usuarios);
  }

  async presentToast( message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  favorite(item){
    this.presentToast('Agregado a favoritos');
    this.lista.closeSlidingItems();
  }
  
  share(item){
    this.presentToast('Agregado a compartidos');
    this.lista.closeSlidingItems();
  }
  
  trash(item){
    this.presentToast('Eliminado');
    this.lista.closeSlidingItems();
  }
  


}
